package com.example.jbeck.class07;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
  public static final String EXTRA_LIST = "com.example.jbeck.class07.LIST";
  public static final String SEPARATOR = ",";
  String mList;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mList = getIntent().getStringExtra(EXTRA_LIST);
    if (mList == null) {
      mList = "";
    }
  }

  public void addItem(View view) {
    EditText editText = (EditText) findViewById(R.id.editText);
    String item = editText.getText().toString();

    if (mList.length() == 0) {
      mList = item;
    }
    else {
      mList += (SEPARATOR + item);
    }

    Intent intent = new Intent(this, DisplayListActivity.class);
    intent.putExtra(EXTRA_LIST, mList);
    startActivity(intent);
  }
}
