package com.example.jbeck.class07;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DisplayListActivity extends AppCompatActivity {
  String mList;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_display_list);

    mList = getIntent().getStringExtra(MainActivity.EXTRA_LIST);

    TextView textView = findViewById(R.id.rawListValue);
    textView.setText(mList);

    String[] itemList = mList.split(MainActivity.SEPARATOR);
    ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.listview_item, itemList);

    ListView listView = (ListView) findViewById(R.id.listView);
    listView.setAdapter(adapter);
  }

  public void addAnotherItem(View view) {
    Intent intent = new Intent(this, MainActivity.class);
    intent.putExtra(MainActivity.EXTRA_LIST, mList);
    startActivity(intent);
  }
}
